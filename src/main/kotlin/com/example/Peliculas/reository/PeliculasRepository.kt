package com.example.Peliculas.reository

import com.example.Peliculas.model.PeliculasModel
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface PeliculasRepository : JpaRepository<PeliculasModel, Long> {

}