package com.example.Peliculas.reository

import com.example.Peliculas.model.UsuariosModel
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.query.Param
import org.springframework.stereotype.Repository
import java.util.*

@Repository
interface UsuariosRepository:JpaRepository<UsuariosModel,Long> {


    @Query("SELECT COUNT(u) FROM USUARIOS u WHERE trim(u.USUARIO) = trim(:usuario) and trim(u.CONTRASENA) = trim(:contrasena) ",nativeQuery = true)
    fun cantidadUsuarioByCredentials(
        @Param("usuario") usuario: String?,
        @Param("contrasena") contrasena:String?
    ): Int


    @Query("SELECT u.* FROM USUARIOS u WHERE trim(u.USUARIO) = trim(:usuario) and trim(u.CONTRASENA) = trim(:contrasena) ",nativeQuery = true)
    fun obtenerUsuario(
        @Param("usuario") usuario: String?,
        @Param("contrasena") contrasena:String?
    ): UsuariosModel?

    @Query("SELECT COUNT (u) FROM USUARIOS u WHERE trim(upper(u.USUARIO)) = trim(upper(:usuario)) ",nativeQuery = true)
    fun cantidadUsuariosByUser(
        @Param("usuario") usuario: String?,
    ): Int


}