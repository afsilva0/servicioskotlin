package com.example.Peliculas.reository

import com.example.Peliculas.model.RolesModel
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository


@Repository
interface RolesRepository :JpaRepository<RolesModel,Long>{
}