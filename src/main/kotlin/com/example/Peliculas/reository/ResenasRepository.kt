package com.example.Peliculas.reository

import com.example.Peliculas.model.ResenasModel
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface ResenasRepository: JpaRepository<ResenasModel,Long> {
}