package com.example.Peliculas.model

import lombok.Data
import javax.persistence.*

@Data
@Entity
@Table(name = "CATEGORIAS")
class CategoriaModel {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    var id: Long? = null

    @Column(name = "NOMBRE")
    var nombre: String? = null
}