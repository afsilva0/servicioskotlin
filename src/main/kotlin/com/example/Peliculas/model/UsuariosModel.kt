package com.example.Peliculas.model

import lombok.Data
import javax.persistence.*

@Data
@Entity
@Table(name = "USUARIOS")
class UsuariosModel {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    var id: Long? = null

    @Column(name = "USUARIO")
    var  usuario: String? = null

    @Column(name = "CONTRASENA")
    var contrasena: String? = null

    @Column(name = "NOMBRES")
    var nombres: String? = null

    @Column(name = "APELLIDOS")
    var apellidos: String? = null

    @Column(name = "ROLID")
    var rolId:Long? =null



}