package com.example.Peliculas.model

import lombok.Data
import java.sql.Timestamp
import javax.persistence.*


@Data
@Entity
@Table(name = "RESENAS")
class ResenasModel {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    var id: Long? = null

    @Column(name = "PELICULAID")
    var peliculaId: Long? = null

    @Column(name = "USUARIOID")
    var usuarioId: Long? = null

    @Column(name = "FECHA")
    var fecha: Timestamp? = null

    @Column(name = "CALIFICACION")
    var calificacion: Number? = null

    @Column(name = "COMENTARIO")
    var comentario: String? = null

}