package com.example.Peliculas.model

import lombok.Data
import java.sql.Timestamp
import javax.persistence.*

@Data
@Entity
@Table(name = "PELICULAS")
class PeliculasModel {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    var id: Long? = null

    @Column(name = "NOMBRE")
    var nombre: String? = null

    @Column(name = "DESCRIPCION")
    var descripcion: String? = null

    @Column(name = "FECHA")
    var fecha: Timestamp? = null

    @Column(name = "IMAGEN")
    var imagen: String? = null

    @Column(name = "TRAILER")
    var trailer: String? = null

    @Column(name = "DIRECTOR")
    var director: String? = null

    @Column(name = "CATEGORIAID")
    var categoriaId: Long? = null







}