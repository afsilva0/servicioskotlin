package com.example.Peliculas.model

import lombok.Data
import javax.persistence.*


@Data
@Entity
@Table(name = "ROLES")
class RolesModel {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    var id: Long? = null

    @Column(name = "TIPOROL")
    var tipoRol: String? = null
}