package com.example.Peliculas.service

import com.example.Peliculas.model.ResenasModel

interface ResenaService {

    fun saveResena(resenasModel: ResenasModel): String
    fun updateResena(resenasModel: ResenasModel): String
    fun getResenasByPelicula(idPelicula: Long): List<ResenasModel>
}
