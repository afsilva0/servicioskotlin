package com.example.Peliculas.service

import com.example.Peliculas.model.CategoriaModel

interface CategoriaService {

    fun saveCategoria(categoriaModel: CategoriaModel): String
    fun getCategorias(): List<CategoriaModel>
    fun updateCategoria(categoriaModel: CategoriaModel): String
}
