package com.example.Peliculas.service

import com.example.Peliculas.model.UsuariosModel

interface UsuariosService {

    fun saveUser(usuariosModel: UsuariosModel): String
    fun getallUser(): List<UsuariosModel>
    fun updateUser(usuariosModel: UsuariosModel): String
    fun obtenerUsuario(usuariosModel: UsuariosModel): UsuariosModel?
    fun registrarUser(usuariosModel: UsuariosModel): String
}
