package com.example.Peliculas.service

import com.example.Peliculas.model.PeliculasModel

interface PeliculasService {

    fun savePelicula(pelicula: PeliculasModel): String

    fun updatePelicula(pelicula: PeliculasModel): String
    fun getPelicula(): List<PeliculasModel>
    fun deletePelicula(id: Long): String
}