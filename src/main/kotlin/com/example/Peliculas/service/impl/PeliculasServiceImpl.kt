package com.example.Peliculas.service.impl

import com.example.Peliculas.model.PeliculasModel
import com.example.Peliculas.reository.PeliculasRepository
import com.example.Peliculas.service.PeliculasService
import org.springframework.stereotype.Service

@Service
class PeliculasServiceImpl(val peliculasRepository: PeliculasRepository) : PeliculasService {

    override fun savePelicula(pelicula: PeliculasModel): String {

        peliculasRepository.save(pelicula)
        return "guardado"
    }

    override fun updatePelicula(pelicula: PeliculasModel):String{

        peliculasRepository.save(pelicula);
        return "actualizado"
    }

    override fun getPelicula():List<PeliculasModel>{
        return peliculasRepository.findAll()

    }

    override fun deletePelicula(id:Long):String{
        peliculasRepository.deleteById(id)
        return "eliminado"
    }


}