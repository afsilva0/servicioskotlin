package com.example.Peliculas.service.impl

import com.example.Peliculas.model.UsuariosModel
import com.example.Peliculas.reository.UsuariosRepository
import com.example.Peliculas.service.UsuariosService
import org.springframework.stereotype.Service


@Service
class UsuariosServiceImpl(val usuariosRepository: UsuariosRepository) : UsuariosService {


    override fun saveUser(usuariosModel: UsuariosModel): String {
        usuariosRepository.save(usuariosModel)
        return "usuario guardado"

    }

    override fun getallUser(): List<UsuariosModel> {

        return usuariosRepository.findAll()
    }

    override fun updateUser(usuariosModel: UsuariosModel): String {
        usuariosRepository.save(usuariosModel)
        return "usuario actualizado"
    }


    override fun registrarUser(usuariosModel: UsuariosModel):String{

        if(existeUsuarioByUser(usuariosModel.usuario.toString())){
            return "este nombre de usuario ya existe"
        }
        usuariosRepository.save(usuariosModel)
        return "se ha registrado el usuario"

    }

    fun existeUsuarioByUser(usuario: String):Boolean{
        var numero = usuariosRepository.cantidadUsuariosByUser(usuario)

        if (numero > 0) {
            return true
        }

        return false
    }


    override fun obtenerUsuario(usuariosModel: UsuariosModel): UsuariosModel? {

//        val existe = this.existeUsuarioByCredentials(usuariosModel.usuario.toString(), usuariosModel.contrasena.toString())

//        if (existe) {
            return usuariosRepository.obtenerUsuario(usuariosModel.usuario, usuariosModel.contrasena)
//        }
//        return null
    }

    private fun existeUsuarioByCredentials(usuario: String, contrasena: String): Boolean {

        var numero = usuariosRepository.cantidadUsuarioByCredentials(usuario, contrasena)

        if (numero > 0) {
            return true
        }
        return false
    }

}