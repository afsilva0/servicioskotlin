package com.example.Peliculas.service.impl

import com.example.Peliculas.model.ResenasModel
import com.example.Peliculas.reository.ResenasRepository
import com.example.Peliculas.service.ResenaService
import org.springframework.stereotype.Service


@Service
class ResenaServiceImpl(val resenasRepository: ResenasRepository): ResenaService {


    override fun saveResena(resenasModel: ResenasModel):String{
        resenasRepository.save(resenasModel)
        return "se ha guardado la reseña"
    }

    override fun updateResena(resenasModel: ResenasModel):String{
        resenasRepository.save(resenasModel)
        return "se ha actualizado la reseña"
    }

    override fun getResenasByPelicula(idPelicula:Long):List<ResenasModel>{
       return resenasRepository.findAll().filter { resenasModel -> resenasModel.peliculaId.toString().equals(idPelicula) }
    }
}