package com.example.Peliculas.service.impl

import com.example.Peliculas.model.CategoriaModel
import com.example.Peliculas.reository.CategoriaRepository
import com.example.Peliculas.service.CategoriaService
import org.springframework.stereotype.Service

@Service
class CategoriaServiceImpl(val categoriaRepository: CategoriaRepository): CategoriaService {


    override fun saveCategoria(categoriaModel: CategoriaModel):String{
        categoriaRepository.save(categoriaModel);
        return "se ha guardado la categoria"

    }

    override fun getCategorias():List<CategoriaModel>{
        return categoriaRepository.findAll()
    }

    override fun updateCategoria(categoriaModel: CategoriaModel):String{
        categoriaRepository.save(categoriaModel)
        return "categoria actualizada"
    }




}