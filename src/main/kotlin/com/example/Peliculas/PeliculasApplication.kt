package com.example.Peliculas

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class PeliculasApplication

fun main(args: Array<String>) {
	runApplication<PeliculasApplication>(*args)
}
