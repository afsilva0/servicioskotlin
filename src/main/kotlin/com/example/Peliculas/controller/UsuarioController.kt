package com.example.Peliculas.controller

import com.example.Peliculas.model.PeliculasModel
import com.example.Peliculas.model.RolesModel
import com.example.Peliculas.model.UsuariosModel
import com.example.Peliculas.service.UsuariosService
import io.swagger.annotations.ApiOperation
import io.swagger.annotations.ApiResponse
import io.swagger.annotations.ApiResponses
import org.springframework.web.bind.annotation.*


@RestController
@CrossOrigin
@RequestMapping("usuarios")
class UsuarioController(val usuariosService: UsuariosService) {


    @ApiOperation(value = "Insert usuario", response = PeliculasModel::class)
    @ApiResponses(
        value = [ApiResponse(code = 200, message = "OK"), ApiResponse(
            code = 400,
            message = "Something Went Wrong"
        ), ApiResponse(code = 500, message = "Internal Server Error")]
    )
    @PostMapping("create")
    fun create(@RequestBody usuariosModel: UsuariosModel):String{
        return usuariosService.saveUser(usuariosModel)

    }


    @ApiOperation(value = "obtener usuario", response = PeliculasModel::class)
    @ApiResponses(
        value = [ApiResponse(code = 200, message = "OK"), ApiResponse(
            code = 400,
            message = "Something Went Wrong"
        ), ApiResponse(code = 500, message = "Internal Server Error")]
    )
    @PostMapping("obtenerUser")
    fun obtenerUsuario(@RequestBody usuariosModel: UsuariosModel):UsuariosModel?{
        return usuariosService.obtenerUsuario(usuariosModel)

    }


    @ApiOperation(value = "Registrar usuario", response = PeliculasModel::class)
    @ApiResponses(
        value = [ApiResponse(code = 200, message = "OK"), ApiResponse(
            code = 400,
            message = "Something Went Wrong"
        ), ApiResponse(code = 500, message = "Internal Server Error")]
    )
    @PostMapping("registrar")
    fun registrar(@RequestBody usuariosModel: UsuariosModel):String{
        return usuariosService.registrarUser(usuariosModel)

    }


    @ApiOperation(value = "update usuario", response = PeliculasModel::class)
    @ApiResponses(
        value = [ApiResponse(code = 200, message = "OK"), ApiResponse(
            code = 400,
            message = "Something Went Wrong"
        ), ApiResponse(code = 500, message = "Internal Server Error")]
    )
    @PostMapping("update")
    fun update(@RequestBody usuariosModel: UsuariosModel):String{
        return usuariosService.updateUser(usuariosModel)

    }


}