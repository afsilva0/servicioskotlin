package com.example.Peliculas.controller

import com.example.Peliculas.model.CategoriaModel
import com.example.Peliculas.model.PeliculasModel
import com.example.Peliculas.service.CategoriaService
import io.swagger.annotations.ApiOperation
import io.swagger.annotations.ApiResponse
import io.swagger.annotations.ApiResponses
import org.springframework.web.bind.annotation.*


@RestController
@RequestMapping("categorias")
@CrossOrigin
class CategoriasController(val categoriaService: CategoriaService){

    @ApiOperation(value = "Insert categoria", response = PeliculasModel::class)
    @ApiResponses(
        value = [ApiResponse(code = 200, message = "OK"), ApiResponse(
            code = 400,
            message = "Something Went Wrong"
        ), ApiResponse(code = 500, message = "Internal Server Error")]
    )
    @PostMapping("create")
    fun create(@RequestBody categoriaModel: CategoriaModel):String{
        return categoriaService.saveCategoria(categoriaModel)

    }

    @ApiOperation(value = "update categoria", response = PeliculasModel::class)
    @ApiResponses(
        value = [ApiResponse(code = 200, message = "OK"), ApiResponse(
            code = 400,
            message = "Something Went Wrong"
        ), ApiResponse(code = 500, message = "Internal Server Error")]
    )
    @PostMapping("update")
    fun update(@RequestBody categoriaModel: CategoriaModel):String{
        return categoriaService.updateCategoria(categoriaModel)

    }

    @ApiOperation(value = "get categorias", response = PeliculasModel::class)
    @ApiResponses(
        value = [ApiResponse(code = 200, message = "OK"), ApiResponse(
            code = 400,
            message = "Something Went Wrong"
        ), ApiResponse(code = 500, message = "Internal Server Error")]
    )
    @GetMapping("getCategorias")
    fun getCategorias():List<CategoriaModel>{
        return categoriaService.getCategorias()

    }

}