package com.example.Peliculas.controller

import com.example.Peliculas.model.PeliculasModel
import com.example.Peliculas.model.RolesModel
import com.example.Peliculas.service.RolesServie
import io.swagger.annotations.ApiOperation
import io.swagger.annotations.ApiResponse
import io.swagger.annotations.ApiResponses
import org.springframework.web.bind.annotation.*


@RestController
@RequestMapping("roles")
@CrossOrigin
class RolesController(val rolesServie: RolesServie){

    @ApiOperation(value = "Insert rol", response = PeliculasModel::class)
    @ApiResponses(
        value = [ApiResponse(code = 200, message = "OK"), ApiResponse(
            code = 400,
            message = "Something Went Wrong"
        ), ApiResponse(code = 500, message = "Internal Server Error")]
    )
    @PostMapping("create")
    fun create(@RequestBody rolesModel: RolesModel):String{
        return rolesServie.guardarRol(rolesModel)

    }
}