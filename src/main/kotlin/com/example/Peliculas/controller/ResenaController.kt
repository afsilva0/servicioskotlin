package com.example.Peliculas.controller

import com.example.Peliculas.model.PeliculasModel
import com.example.Peliculas.model.ResenasModel
import com.example.Peliculas.service.ResenaService
import io.swagger.annotations.ApiOperation
import io.swagger.annotations.ApiResponse
import io.swagger.annotations.ApiResponses
import org.springframework.web.bind.annotation.*


@RestController
@RequestMapping("resena")
@CrossOrigin
class ResenaController(val resenaService: ResenaService) {

    @ApiOperation(value = "Insert resena", response = PeliculasModel::class)
    @ApiResponses(
        value = [ApiResponse(code = 200, message = "OK"), ApiResponse(
            code = 400,
            message = "Something Went Wrong"
        ), ApiResponse(code = 500, message = "Internal Server Error")]
    )
    @PostMapping("create")
    fun create(@RequestBody resenasModel: ResenasModel):String{
        return resenaService.saveResena(resenasModel)

    }
    @ApiOperation(value = "update resena", response = PeliculasModel::class)
    @ApiResponses(
        value = [ApiResponse(code = 200, message = "OK"), ApiResponse(
            code = 400,
            message = "Something Went Wrong"
        ), ApiResponse(code = 500, message = "Internal Server Error")]
    )
    @PostMapping("update")
    fun update(@RequestBody resenasModel: ResenasModel):String{
        return resenaService.updateResena(resenasModel)

    }

    @ApiOperation(value = "get reseñas por pelicula", response = PeliculasModel::class)
    @ApiResponses(
        value = [ApiResponse(code = 200, message = "OK"), ApiResponse(
            code = 400,
            message = "Something Went Wrong"
        ), ApiResponse(code = 500, message = "Internal Server Error")]
    )
    @GetMapping("getResenasBy/{idPelicula}")
    fun update(@PathVariable("idPelicula") id:Long):List<ResenasModel>{
        return resenaService.getResenasByPelicula(id)

    }
}