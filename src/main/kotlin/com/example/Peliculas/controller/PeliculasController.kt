package com.example.Peliculas.controller

import com.example.Peliculas.model.PeliculasModel
import com.example.Peliculas.service.PeliculasService
import io.swagger.annotations.ApiOperation
import io.swagger.annotations.ApiResponse
import io.swagger.annotations.ApiResponses
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("peliculas")
@CrossOrigin
class PeliculasController(val peliculasService: PeliculasService){

    @ApiOperation(value = "Insert Pelicula", response = PeliculasModel::class)
    @ApiResponses(
        value = [ApiResponse(code = 200, message = "OK"), ApiResponse(
            code = 400,
            message = "Something Went Wrong"
        ), ApiResponse(code = 500, message = "Internal Server Error")]
    )
    @PostMapping("create")
    fun create(@RequestBody pelicula: PeliculasModel):String{
    return peliculasService.savePelicula(pelicula)

    }

    @ApiOperation(value = "update Pelicula", response = PeliculasModel::class)
    @ApiResponses(
        value = [ApiResponse(code = 200, message = "OK"), ApiResponse(
            code = 400,
            message = "Something Went Wrong"
        ), ApiResponse(code = 500, message = "Internal Server Error")]
    )
    @PostMapping("update")
    fun update(@RequestBody pelicula: PeliculasModel):String{
        return peliculasService.updatePelicula(pelicula)

    }


    @ApiOperation(value = "get Peliculas", response = PeliculasModel::class)
    @ApiResponses(
        value = [ApiResponse(code = 200, message = "OK"), ApiResponse(
            code = 400,
            message = "Something Went Wrong"
        ), ApiResponse(code = 500, message = "Internal Server Error")]
    )
    @GetMapping("getPeliculas")
    fun getPeliculas():List<PeliculasModel>{
        return peliculasService.getPelicula()

    }


    @ApiOperation(value = "delete pelicula by id", response = PeliculasModel::class)
    @ApiResponses(
        value = [ApiResponse(code = 200, message = "OK"), ApiResponse(
            code = 400,
            message = "Something Went Wrong"
        ), ApiResponse(code = 500, message = "Internal Server Error")]
    )
    @GetMapping("deletePelicula/{id}")
    fun deletePelicula(@PathVariable("id") id:Long):String{
        return peliculasService.deletePelicula(id)
    }


}